import React, {Component} from 'react';
import {Typefont} from '../typefont/src/index.js';

class Fontfamily extends Component {
    constructor(props) {
        super(props);
        this.state = { data: [] };
    }


    useCroppedImage() {
        const image = document.getElementById('croppedImage');
        Typefont(image.src).then((res) => {
            console.table(res);

        });
    }


    render()
    {
        return <div>
            <button onClick={this.useCroppedImage}>
                font
            </button>
        </div>;

    }
}

export default Fontfamily;