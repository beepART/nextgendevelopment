import * as Vibrant from 'node-vibrant'
import React, {Component} from 'react';


class ColorPalette extends Component {
    constructor(props) {
        super(props);
        this.state = { data: [] };

    }


    getColorPalette(palette) {
        let hexPalette = [];
        let color = {};

        Object.keys(palette).forEach(function (key) {
            color.name = key;
            color.heyValue = palette[key]._rgb;
            hexPalette.push({color: {key: key, hexValue: palette[key].hex}});

        });
        return hexPalette;
    }

    createPalette() {
        console.log("test");
        const image = document.getElementById('croppedImage');
        if(image) {
            console.log(image);
            Vibrant.from(image).getPalette()
                .then((palette) => {
                    console.log(palette);
                    this.getColorPalette(palette);
                });
        }else {
            console.error("no image file");
        }
    }


    render()
    {
        return <div>
            <p>hello</p>
            <button onClick={this.createPalette.bind(this)}>
                    test
            </button>
        </div>;

    }
}

export default ColorPalette;